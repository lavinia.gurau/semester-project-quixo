let table;
let w;
const cols = 5;
const rows = 5;
let array = new Array();
let button1;
let button2;
let player1;
let player2;
let resetButton;
let firstPlayerDiv;
let vs;
let secondPlayerDiv;
let player1Symbol;
let selectedSymbolPlayer1;
let selectedSymbolPlayer2;
let sel1;
let sel2;

function setup() {
  createCanvas(620, 620);
  rectangle.initiate();
  createFirstPlayerDiv();
  addFirstPlayer();
  createButton1();
  createSecondPlayerDiv();
  addSecondPlayer();
  createButton2();
  createResetButton();
  addFirstPlayerSymbol();
  addSecondPlayerSymbol();
  w = floor(width / cols);
  table = makeTable(cols, rows);
  for (let i = 0; i < cols; i++) {
    for (let j = 0; j < rows; j++) {
      table[i][j] = new Rect(i, j, w, null, null);
      array.push(new Rect(i, j, w, null, null));
    }
  }
}

class Rect {

  initiate(rectangle = null) {
    if (rectangle) {
      this.rect = rectangle;
    } else {
      this.rectangle = [];
      for (let i = 0; i < 5; i++) {
        let rand = [];
        for (let j = 0; j < 5; j++) {
          let valueObj = {
            value: '',
            color: "rgb(255, 230, 179)"
          }
          rand.push(valueObj);
        }
        this.rectangle.push(rand);
      }
    }
  }

  display(xInitiate, y) {
    let x = xInitiate;
    for (var i = 0; i < 5; i++) {
      x = xInitiate;
      for (var j = 0; j < 5; j++) {
        stroke("black");
        strokeWeight(1);
        fill(this.rectangle[i][j].color);
        rect(x, y, 100, 100);
        textSize(100);
        fill("black")
        text(this.rectangle[i][j].value, x + 25, y + 75);
        x += 100;
      }

      y += 100;

    }
  }

  click(mousx, mousy) {
    for (let i = 0; i < 5; i++)
      for (let j = 0; j < 5; j++)
        if (mousy >= 50 + 100 * i && mousy <= 150 + 100 * i && mousx >= 50 + 100 * j && mousx <= 150 + 100 * j) {
          var thing = true;
          if (thing == true) {
            this.rectangle[i][j].value = sel1.value();
            thing = false;
          } else if(thing == false) {
            this.rectangle[i][j].value = sel2.value();
          }
        }
  }


}

function makeTable(cols, rows) {
  let colsArray = new Array(cols);
  for (let i = 0; i < colsArray.length; i++) {
    colsArray[i] = new Array(rows);
  }
  return colsArray;
}



function mouseClicked() {
  rectangle.click(mouseX, mouseY);
}

var rectangle = new Rect();


function draw() {
  background(250);

  background("rgb(128, 128, 0)");

  rectangle.display(50, 50);
}

function createResetButton() {
  buttonColor = color("orange");
  resetButton = createButton('Reset');
  resetButton.position(1050, 115);
  resetButton.size(100);
  resetButton.style('background-color', this.buttonColor);
  resetButton.mousePressed(resetForm);
}

function resetForm() {
  addFirstPlayer();
  addSecondPlayer();
  if (firstPlayerDiv && vs && secondPlayerDiv) {
    firstPlayerDiv.remove();
    vs.remove();
    secondPlayerDiv.remove();
  }
  setup();
}

function addFirstPlayer() {
  let inp = createInput('');
  inp.position(780, 100);
  inp.size(100);
  inp.input(myInputEvent1);
}

function createFirstPlayerDiv() {
  let div = createDiv('First Player:');
  div.style('font-size', '16px');
  div.position(650, 100);
}

function createButton1() {
  buttonColor = color("lightblue");
  button1 = createButton('Add First Player');
  button1.position(900, 100);
  button1.size(135);
  button1.style('background-color', this.buttonColor);
  button1.mousePressed(displayPlayer1);
}

function myInputEvent1() {
  player1 = this.value();
}

function myInputEvent2() {
  player1Symbol = this.value();
}

function displayPlayer1() {
  if (player1 != null) {
      firstPlayerDiv = createDiv(player1);
      firstPlayerDiv.style('font-size', '36px');
      firstPlayerDiv.position(800, 200);
  }
}

function addFirstPlayerSymbol() {
  let div = createDiv('First Player Symbol:');
  div.style('font-size', '16px');
  div.position(650, 500);
  sel1 = createSelect();
  sel1.position(800, 500);
  sel1.option('x');
  sel1.option('0');
}

function addSecondPlayer() {
  let inp = createInput('');
  inp.position(780, 130);
  inp.size(100);
  inp.input(myInputEvent2);
}

function createSecondPlayerDiv() {
  let div = createDiv('Second Player:');
  div.style('font-size', '16px');
  div.position(650, 130);
}

function createButton2() {
  buttonColor = color("lightblue");
  button2 = createButton('Add Second Player');
  button2.position(900, 130);
  button2.size(135);
  button2.style('background-color', this.buttonColor);
  button2.mousePressed(displayPlayer2);
}

function myInputEvent2() {
  player2 = this.value();
}

function displayPlayer2() {
  if (player2 != null) {
      vs = createDiv("VS");
      secondPlayerDiv = createDiv(player2);
      secondPlayerDiv.style('font-size', '36px');
      vs.style('font-size', '42px');
      vs.style('color', 'darkred');
      vs.position(800, 300);
      secondPlayerDiv.position(800, 400);
  }
}

function addSecondPlayerSymbol() {
  let div = createDiv('Second Player Symbol:');
  div.style('font-size', '16px');
  div.position(650, 530);
  sel2 = createSelect();
  sel2.position(800, 530);
  sel2.option('x');
  sel2.option('0');
}
